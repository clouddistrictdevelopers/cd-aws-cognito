# ------------------------------------------------------------------------------
# https://github.com/mineiros-io/terraform-aws-cognito-user-pool
# OUTPUT CALCULATED VARIABLES (prefer full objects)
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# OUTPUT ALL RESOURCES AS FULL OBJECTS
# ------------------------------------------------------------------------------

# fix tf13 output diff
locals {
  user_pool = try(aws_cognito_user_pool.user_pool[0], {})

  o_user_pool_tags = try(local.user_pool.tags, {})

  o_user_pool = var.module_enabled ? merge(local.user_pool, {
    tags = local.o_user_pool_tags != null ? local.user_pool.tags : {}
  }) : null
}


output "user_pool" {
  description = "The full `aws_cognito_user_pool` object."
  value       = local.o_user_pool
}

output "domain" {
  description = "The full `aws_cognito_user_pool` object."
  value       = try(aws_cognito_user_pool_domain.domain[0], null)
}

output "clients" {
  description = "All Cognito User Pool Client resources associated with the Cognito User Pool."
  value       = { for client in aws_cognito_user_pool_client.client : client.name => merge(client, { client_secret = null }) }
}

output "client_secrets" {
  description = "The secrets of all created Cognito User Pool Client resources."
  value       = { for client in aws_cognito_user_pool_client.client : client.name => client.client_secret }
  sensitive   = true
}

# ------------------------------------------------------------------------------
# OUTPUT ALL INPUT VARIABLES
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# OUTPUT MODULE CONFIGURATION
# ------------------------------------------------------------------------------

output "module_enabled" {
  description = "Whether the module is enabled"
  value       = var.module_enabled
}

# output "app_user_name" {
#   value = aws_iam_user.cognito_app_user.name
# }
#
# output "app_user_arn" {
#   value = aws_iam_user.cognito_app_user.arn
# }

// output "acm_certificate_arn" {
//   value = aws_acm_certificate.certificate.arn
// }

// output "acm_certificate_domain_name" {
//   value = aws_acm_certificate.certificate.domain_name
// }

// output "acm_certificate_domain_validation_options" {
//   value = aws_acm_certificate.certificate.domain_validation_options
// }

# output "identity_pool_id" {
#   value = aws_cognito_identity_pool.apps_identity_pool.id
# }
#
# output "identity_pool_arn" {
#   value = aws_cognito_identity_pool.apps_identity_pool.arn
# }
#
# output "identity_pool_authenticated_id" {
#   value = aws_iam_role.apps_identity_pool_authenticated.id
# }
#
# output "identity_pool_authenticated_arn" {
#   value = aws_iam_role.apps_identity_pool_authenticated.arn
# }
#
# output "identity_pool_unauthenticated_id" {
#   value = aws_iam_role.apps_identity_pool_unauthenticated.id
# }
#
# output "identity_pool_unauthenticated_arn" {
#   value = aws_iam_role.apps_identity_pool_unauthenticated.arn
# }
