locals {
  identity_pool = try( aws_cognito_identity_pool.main[0], null )
}

output "identity_pool" {
  description = "Identity pool full data"
  value       = local.identity_pool
}

