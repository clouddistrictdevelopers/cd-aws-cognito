module "aws_cognito" {
  source = "./modules/cognito_user_pool"

  aws_region = var.aws_region
  environment = var.environment

  module_enabled = var.module_enabled

  name                     = var.user_pool_name
  alias_attributes         = var.alias_attributes
  username_attributes      = var.username_attributes
  auto_verified_attributes = var.auto_verified_attributes

  sms_authentication_message = var.sms_authentication_message

  mfa_configuration = var.mfa_configuration

  password_minimum_length          = var.password_minimum_length
  password_require_numbers         = var.password_require_numbers
  password_require_symbols         = var.password_require_symbols
  password_require_uppercase       = var.password_require_uppercase
  password_require_lowercase       = var.password_require_lowercase
  temporary_password_validity_days = var.temporary_password_validity_days

  account_recovery_mechanisms = var.account_recovery_mechanisms

  user_device_tracking = var.user_device_tracking
  challenge_required_on_new_device = var.challenge_required_on_new_device

  allow_software_mfa_token = var.allow_software_mfa_token

  enable_username_case_sensitivity = var.enable_username_case_sensitivity

  email_sending_account  = var.email_sending_account
  email_reply_to_address = var.email_reply_to_address
  email_source_arn       = var.email_source_arn
  email_from_address     = var.email_from_address

  allow_admin_create_user_only = var.allow_admin_create_user_only
  invite_email_subject = var.invite_email_subject
  invite_email_message = var.invite_email_message
  invite_sms_message   = var.invite_sms_message

  schema_attributes = var.schema_attributes

  lambda_create_auth_challenge          = var.lambda_create_auth_challenge
  lambda_custom_message                 = var.lambda_custom_message
  lambda_define_auth_challenge          = var.lambda_define_auth_challenge
  lambda_post_authentication            = var.lambda_post_authentication
  lambda_post_confirmation              = var.lambda_post_confirmation
  lambda_pre_authentication             = var.lambda_pre_authentication
  lambda_pre_sign_up                    = var.lambda_pre_sign_up
  lambda_pre_token_generation           = var.lambda_pre_token_generation
  lambda_user_migration                 = var.lambda_user_migration
  lambda_verify_auth_challenge_response = var.lambda_verify_auth_challenge_response

  sms_configuration = var.sms_configuration

  advanced_security_mode = var.advanced_security_mode

  default_email_option  = var.default_email_option
  email_subject         = var.email_subject
  email_message         = var.email_message
  email_subject_by_link = var.email_subject_by_link
  email_message_by_link = var.email_message_by_link
  sms_message           = var.sms_message

  tags = var.tags

  clients = var.clients
  default_client_allowed_oauth_flows                  = var.default_client_allowed_oauth_flows
  default_client_allowed_oauth_flows_user_pool_client = var.default_client_allowed_oauth_flows_user_pool_client
  default_client_allowed_oauth_scopes                 = var.default_client_allowed_oauth_scopes
  default_client_callback_urls                        = var.default_client_callback_urls
  default_client_default_redirect_uri                 = var.default_client_default_redirect_uri
  default_client_explicit_auth_flows                  = var.default_client_explicit_auth_flows
  default_client_generate_secret                      = var.default_client_generate_secret
  default_client_logout_urls                          = var.default_client_logout_urls
  default_client_read_attributes                      = var.default_client_read_attributes
  default_client_refresh_token_validity               = var.default_client_refresh_token_validity
  default_client_supported_identity_providers         = var.default_client_supported_identity_providers
  default_client_prevent_user_existence_errors        = var.default_client_prevent_user_existence_errors
  default_client_write_attributes                     = var.default_client_write_attributes

  domain          = var.domain
  certificate_arn = var.certificate_arn
}

module "cognito_identity_pool" {
  source = "./modules/cognito_identity"

  identity_pool_enabled = var.identity_pool_enabled

  # Identity Pool
  identity_pool_name = var.identity_pool_name
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  client_id = lookup(module.aws_cognito.clients, element(var.clients, 0).name, "").id
  provider_name = module.aws_cognito.user_pool.endpoint
}
