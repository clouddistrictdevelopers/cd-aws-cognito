## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.8 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 2 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 2 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_aws_cognito"></a> [aws\_cognito](#module\_aws\_cognito) | ./modules/cognito_user_pool | n/a |
| <a name="module_cognito_identity_pool"></a> [cognito\_identity\_pool](#module\_cognito\_identity\_pool) | ./modules/cognito_identity | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_recovery_mechanisms"></a> [account\_recovery\_mechanisms](#input\_account\_recovery\_mechanisms) | (Optional) A list of recovery\_mechanisms which are defined by a `name` and its `priority`. Valid values for `name`<br>are verified\_email, verified\_phone\_number, and admin\_only. | `any` | `[]` | no |
| <a name="input_advanced_security_mode"></a> [advanced\_security\_mode](#input\_advanced\_security\_mode) | (Optional) The mode for advanced security, must be one of `OFF`, `AUDIT` or `ENFORCED`. Additional pricing applies for Amazon Cognito advanced security features. For details see https://aws.amazon.com/cognito/pricing/ | `string` | `"OFF"` | no |
| <a name="input_alias_attributes"></a> [alias\_attributes](#input\_alias\_attributes) | (Optional) Attributes supported as an alias for this user pool. Possible values: 'phone\_number', 'email', or 'preferred\_username'. Conflicts with username\_attributes. | `set(string)` | `null` | no |
| <a name="input_allow_admin_create_user_only"></a> [allow\_admin\_create\_user\_only](#input\_allow\_admin\_create\_user\_only) | (Optional) Set to True if only the administrator is allowed to create user profiles. Set to False if users can sign themselves up via an app. | `bool` | `true` | no |
| <a name="input_allow_software_mfa_token"></a> [allow\_software\_mfa\_token](#input\_allow\_software\_mfa\_token) | (Optional) Boolean whether to enable software token Multi-Factor (MFA) tokens, such as Time-based One-Time Password (TOTP). To disable software token MFA when 'sms\_configuration' is not present, the 'mfa\_configuration' argument must be set to OFF and the 'software\_token\_mfa\_configuration' configuration block must be fully removed. | `bool` | `true` | no |
| <a name="input_allow_unauthenticated_identities"></a> [allow\_unauthenticated\_identities](#input\_allow\_unauthenticated\_identities) | Whether the identity pool supports unauthenticated logins or not.(true or false) | `string` | `false` | no |
| <a name="input_auto_verified_attributes"></a> [auto\_verified\_attributes](#input\_auto\_verified\_attributes) | (Optional) The attributes to be auto-verified. Possible values: 'email', 'phone\_number'. | `set(string)` | <pre>[<br>  "email"<br>]</pre> | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS region to use. | `string` | `"eu-west-1"` | no |
| <a name="input_certificate_arn"></a> [certificate\_arn](#input\_certificate\_arn) | (Optional) The ARN of an ISSUED ACM certificate in us-east-1 for a custom domain. | `string` | `null` | no |
| <a name="input_challenge_required_on_new_device"></a> [challenge\_required\_on\_new\_device](#input\_challenge\_required\_on\_new\_device) | (Optional) Indicates whether a challenge is required on a new device. Only applicable to a new device. | `bool` | `true` | no |
| <a name="input_client_id"></a> [client\_id](#input\_client\_id) | The client ID for the Amazon Cognito Identity User Pool. | `string` | `""` | no |
| <a name="input_clients"></a> [clients](#input\_clients) | (Optional) A list of objects with the clients definitions. | `any` | `[]` | no |
| <a name="input_cognito_identity_providers"></a> [cognito\_identity\_providers](#input\_cognito\_identity\_providers) | An array of Amazon Cognito Identity user pools and their client IDs. | `map` | `{}` | no |
| <a name="input_default_client_allowed_oauth_flows"></a> [default\_client\_allowed\_oauth\_flows](#input\_default\_client\_allowed\_oauth\_flows) | (Optional) List of allowed OAuth flows. Possible flows are 'code', 'implicit', and 'client\_credentials'. | `list(string)` | `null` | no |
| <a name="input_default_client_allowed_oauth_flows_user_pool_client"></a> [default\_client\_allowed\_oauth\_flows\_user\_pool\_client](#input\_default\_client\_allowed\_oauth\_flows\_user\_pool\_client) | (Optional) Whether the client is allowed to follow the OAuth protocol when interacting with Cognito User Pools. | `bool` | `null` | no |
| <a name="input_default_client_allowed_oauth_scopes"></a> [default\_client\_allowed\_oauth\_scopes](#input\_default\_client\_allowed\_oauth\_scopes) | (Optional) List of allowed OAuth scopes. Possible values are 'phone', 'email', 'openid', 'profile', and 'aws.cognito.signin.user.admin'. | `list(string)` | `null` | no |
| <a name="input_default_client_callback_urls"></a> [default\_client\_callback\_urls](#input\_default\_client\_callback\_urls) | (Optional) List of allowed callback URLs for the identity providers. | `list(string)` | `null` | no |
| <a name="input_default_client_default_redirect_uri"></a> [default\_client\_default\_redirect\_uri](#input\_default\_client\_default\_redirect\_uri) | (Optional) The default redirect URI. Must be in the list of callback URLs. | `string` | `null` | no |
| <a name="input_default_client_explicit_auth_flows"></a> [default\_client\_explicit\_auth\_flows](#input\_default\_client\_explicit\_auth\_flows) | (Optional) List of authentication flows. Possible values are 'ADMIN\_NO\_SRP\_AUTH', 'CUSTOM\_AUTH\_FLOW\_ONLY', 'USER\_PASSWORD\_AUTH', 'ALLOW\_ADMIN\_USER\_PASSWORD\_AUTH', 'ALLOW\_CUSTOM\_AUTH', 'ALLOW\_USER\_PASSWORD\_AUTH', 'ALLOW\_USER\_SRP\_AUTH', and 'ALLOW\_REFRESH\_TOKEN\_AUTH'. | `list(string)` | `null` | no |
| <a name="input_default_client_generate_secret"></a> [default\_client\_generate\_secret](#input\_default\_client\_generate\_secret) | (Optional) Boolean flag for generating an application secret. | `bool` | `null` | no |
| <a name="input_default_client_logout_urls"></a> [default\_client\_logout\_urls](#input\_default\_client\_logout\_urls) | (Optional) List of allowed logout URLs for the identity providers. | `list(string)` | `null` | no |
| <a name="input_default_client_prevent_user_existence_errors"></a> [default\_client\_prevent\_user\_existence\_errors](#input\_default\_client\_prevent\_user\_existence\_errors) | (Optional) Choose which errors and responses are returned by Cognito APIs during authentication, account confirmation, and password recovery when the user does not exist in the Cognito User Pool. When set to 'ENABLED' and the user does not exist, authentication returns an error indicating either the username or password was incorrect, and account confirmation and password recovery return a response indicating a code was sent to a simulated destination. When set to 'LEGACY', those APIs will return a 'UserNotFoundException' exception if the user does not exist in the Cognito User Pool. | `string` | `null` | no |
| <a name="input_default_client_read_attributes"></a> [default\_client\_read\_attributes](#input\_default\_client\_read\_attributes) | (Optional) List of Cognito User Pool attributes the application client can read from. | `list(string)` | `null` | no |
| <a name="input_default_client_refresh_token_validity"></a> [default\_client\_refresh\_token\_validity](#input\_default\_client\_refresh\_token\_validity) | (Optional) The time limit in days refresh tokens are valid for. | `number` | `30` | no |
| <a name="input_default_client_supported_identity_providers"></a> [default\_client\_supported\_identity\_providers](#input\_default\_client\_supported\_identity\_providers) | (Optional) List of provider names for the identity providers that are supported on this client. | `list(string)` | `null` | no |
| <a name="input_default_client_write_attributes"></a> [default\_client\_write\_attributes](#input\_default\_client\_write\_attributes) | (Optional) List of Cognito User Pool attributes the application client can write to. | `list(string)` | `null` | no |
| <a name="input_default_email_option"></a> [default\_email\_option](#input\_default\_email\_option) | (Optional) The default email option. Must be either `CONFIRM_WITH_CODE` or `CONFIRM_WITH_LINK`. | `string` | `"CONFIRM_WITH_CODE"` | no |
| <a name="input_developer_provider_name"></a> [developer\_provider\_name](#input\_developer\_provider\_name) | "The domain by which Cognito will refer to your users. This name acts as a placeholder that allows your <br>backend and the Cognito service to communicate about the developer provider." | `string` | `""` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | (Optional) Type a domain prefix to use for the sign-up and sign-in pages that are hosted by Amazon Cognito, e.g. 'https://{YOUR_PREFIX}.auth.eu-west-1.amazoncognito.com'. The prefix must be unique across the selected AWS Region. Domain names can only contain lower-case letters, numbers, and hyphens. | `string` | `null` | no |
| <a name="input_email_configuration_set"></a> [email\_configuration\_set](#input\_email\_configuration\_set) | (Optional) Email configuration set name from SES. | `string` | `null` | no |
| <a name="input_email_from_address"></a> [email\_from\_address](#input\_email\_from\_address) | (Optional) - Sender’s email address or sender’s name with their email address (e.g. 'john@smith.com' or 'John Smith <john@smith.com>'). | `string` | `null` | no |
| <a name="input_email_message"></a> [email\_message](#input\_email\_message) | (Optional) The email message template. Must contain the {####} placeholder. | `string` | `"Your verification code is {####}."` | no |
| <a name="input_email_message_by_link"></a> [email\_message\_by\_link](#input\_email\_message\_by\_link) | (Optional) The email message template for sending a confirmation link to the user, it must contain the {##Click Here##} placeholder. | `string` | `"Please click the link below to verify your email address. {##Verify Email##}."` | no |
| <a name="input_email_reply_to_address"></a> [email\_reply\_to\_address](#input\_email\_reply\_to\_address) | (Optional) - The REPLY-TO email address. | `string` | `null` | no |
| <a name="input_email_sending_account"></a> [email\_sending\_account](#input\_email\_sending\_account) | (Optional) The email delivery method to use. 'COGNITO\_DEFAULT' for the default email functionality built into Cognito or 'DEVELOPER' to use your Amazon SES configuration. | `string` | `"COGNITO_DEFAULT"` | no |
| <a name="input_email_source_arn"></a> [email\_source\_arn](#input\_email\_source\_arn) | (Optional) - The ARN of the email source. | `string` | `null` | no |
| <a name="input_email_subject"></a> [email\_subject](#input\_email\_subject) | (Optional) The subject line for the email message template. | `string` | `"Your Verification Code"` | no |
| <a name="input_email_subject_by_link"></a> [email\_subject\_by\_link](#input\_email\_subject\_by\_link) | (Optional) The subject line for the email message template for sending a confirmation link to the user. | `string` | `"Your Verification Link"` | no |
| <a name="input_enable_username_case_sensitivity"></a> [enable\_username\_case\_sensitivity](#input\_enable\_username\_case\_sensitivity) | (Optional) Specifies whether username case sensitivity will be applied for all users in the user pool through Cognito APIs. | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `any` | n/a | yes |
| <a name="input_identity_pool_enabled"></a> [identity\_pool\_enabled](#input\_identity\_pool\_enabled) | (Optional) Whether to create an identity pool or not. Default is false. | `bool` | `false` | no |
| <a name="input_identity_pool_name"></a> [identity\_pool\_name](#input\_identity\_pool\_name) | The cognito identity pool name | `string` | `null` | no |
| <a name="input_invite_email_message"></a> [invite\_email\_message](#input\_invite\_email\_message) | (Optional) The message template for email messages. Must contain {username} and {####} placeholders, for username and temporary password, respectively. | `string` | `"Your username is {username} and your temporary password is '{####}'."` | no |
| <a name="input_invite_email_subject"></a> [invite\_email\_subject](#input\_invite\_email\_subject) | (Optional) The subject for email messages. | `string` | `"Your new account."` | no |
| <a name="input_invite_sms_message"></a> [invite\_sms\_message](#input\_invite\_sms\_message) | (Optional) The message template for SMS messages. Must contain {username} and {####} placeholders, for username and temporary password, respectively. | `string` | `"Your username is {username} and your temporary password is '{####}'."` | no |
| <a name="input_lambda_create_auth_challenge"></a> [lambda\_create\_auth\_challenge](#input\_lambda\_create\_auth\_challenge) | (Optional) The ARN of an AWS Lambda creating an authentication challenge. | `string` | `null` | no |
| <a name="input_lambda_custom_message"></a> [lambda\_custom\_message](#input\_lambda\_custom\_message) | (Optional) The ARN of a custom message AWS Lambda trigger. | `string` | `null` | no |
| <a name="input_lambda_define_auth_challenge"></a> [lambda\_define\_auth\_challenge](#input\_lambda\_define\_auth\_challenge) | (Optional) The ARN of an AWS Lambda that defines the authentication challenge. | `string` | `null` | no |
| <a name="input_lambda_post_authentication"></a> [lambda\_post\_authentication](#input\_lambda\_post\_authentication) | (Optional) The ARN of a post-authentication AWS Lambda trigger. | `string` | `null` | no |
| <a name="input_lambda_post_confirmation"></a> [lambda\_post\_confirmation](#input\_lambda\_post\_confirmation) | (Optional) The ARN of a post-confirmation AWS Lambda trigger. | `string` | `null` | no |
| <a name="input_lambda_pre_authentication"></a> [lambda\_pre\_authentication](#input\_lambda\_pre\_authentication) | (Optional) The ARN of a pre-authentication AWS Lambda trigger. | `string` | `null` | no |
| <a name="input_lambda_pre_sign_up"></a> [lambda\_pre\_sign\_up](#input\_lambda\_pre\_sign\_up) | (Optional) The ARN of a pre-registration AWS Lambda trigger. | `string` | `null` | no |
| <a name="input_lambda_pre_token_generation"></a> [lambda\_pre\_token\_generation](#input\_lambda\_pre\_token\_generation) | (Optional) The ARN of an AWS Lambda that allows customization of identity token claims before token generation. | `string` | `null` | no |
| <a name="input_lambda_user_migration"></a> [lambda\_user\_migration](#input\_lambda\_user\_migration) | (Optional) The ARN of the user migration AWS Lambda config type. | `string` | `null` | no |
| <a name="input_lambda_verify_auth_challenge_response"></a> [lambda\_verify\_auth\_challenge\_response](#input\_lambda\_verify\_auth\_challenge\_response) | (Optional) The ARN of an AWS Lambda that verifies the authentication challenge response. | `string` | `null` | no |
| <a name="input_mfa_configuration"></a> [mfa\_configuration](#input\_mfa\_configuration) | Multi-Factor Authentication (MFA) configuration for the User Pool. Valid values: 'ON', 'OFF' or 'OPTIONAL'. 'ON' and 'OPTIONAL' require at least one of 'sms\_configuration' or 'software\_token\_mfa\_configuration' to be configured. | `string` | `"OPTIONAL"` | no |
| <a name="input_module_depends_on"></a> [module\_depends\_on](#input\_module\_depends\_on) | (Optional) A list of external resources the module depends\_on. Default is []. | `any` | `[]` | no |
| <a name="input_module_enabled"></a> [module\_enabled](#input\_module\_enabled) | (Optional) Whether to create resources within the module or not. Default is true. | `bool` | `true` | no |
| <a name="input_openid_connect_provider_arns"></a> [openid\_connect\_provider\_arns](#input\_openid\_connect\_provider\_arns) | A list of OpendID Connect provider ARNs. | `list` | `[]` | no |
| <a name="input_password_minimum_length"></a> [password\_minimum\_length](#input\_password\_minimum\_length) | (Optional) The minimum length of the password policy that you have set. | `number` | `20` | no |
| <a name="input_password_require_lowercase"></a> [password\_require\_lowercase](#input\_password\_require\_lowercase) | (Optional) Whether you have required users to use at least one lowercase letter in their password. | `bool` | `true` | no |
| <a name="input_password_require_numbers"></a> [password\_require\_numbers](#input\_password\_require\_numbers) | (Optional) Whether you have required users to use at least one number in their password. | `bool` | `true` | no |
| <a name="input_password_require_symbols"></a> [password\_require\_symbols](#input\_password\_require\_symbols) | (Optional) Whether you have required users to use at least one symbol in their password. | `bool` | `true` | no |
| <a name="input_password_require_uppercase"></a> [password\_require\_uppercase](#input\_password\_require\_uppercase) | (Optional) Whether you have required users to use at least one uppercase letter in their password. | `bool` | `true` | no |
| <a name="input_provider_name"></a> [provider\_name](#input\_provider\_name) | The provider name for an Amazon Cognito Identity User Pool. | `string` | `""` | no |
| <a name="input_saml_provider_arns"></a> [saml\_provider\_arns](#input\_saml\_provider\_arns) | An array of Amazon Resource Names (ARNs) of the SAML provider for your identity. | `list` | `[]` | no |
| <a name="input_schema_attributes"></a> [schema\_attributes](#input\_schema\_attributes) | (Optional) A list of schema attributes of a user pool. You can add a maximum um 25 custom attributes. | `any` | `[]` | no |
| <a name="input_server_side_token_check"></a> [server\_side\_token\_check](#input\_server\_side\_token\_check) | Whether server-side token validation is enabled for the identity provider’s token or not.(true or false) | `bool` | `false` | no |
| <a name="input_sms_authentication_message"></a> [sms\_authentication\_message](#input\_sms\_authentication\_message) | (Optional) A string representing the SMS authentication message. The message must contain the {####} placeholder, which will be replaced with the authentication code. | `string` | `"Your temporary password is {####}."` | no |
| <a name="input_sms_configuration"></a> [sms\_configuration](#input\_sms\_configuration) | (Optional) The `sms_configuration` with the `external_id` parameter used in iam role trust relationships and the `sns_caller_arn` parameter to set he arn of the amazon sns caller. this is usually the iam role that you've given cognito permission to assume. | <pre>object({<br>    # The external ID used in IAM role trust relationships. For more information about using external IDs, see https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user_externalid.html<br>    external_id = string<br>    # The ARN of the Amazon SNS caller. This is usually the IAM role that you've given Cognito permission to assume.<br>    sns_caller_arn = string<br>  })</pre> | `null` | no |
| <a name="input_sms_message"></a> [sms\_message](#input\_sms\_message) | (Optional) The SMS message template. Must contain the {####} placeholder, which will be replaced with the verification code. Can also contain the {username} placeholder which will be replaced with the username. | `string` | `"Your verification code is {####}."` | no |
| <a name="input_supported_login_providers"></a> [supported\_login\_providers](#input\_supported\_login\_providers) | Key-Value pairs mapping provider names to provider app IDs. | `map` | `{}` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) A mapping of tags to assign to the resource. | `map(string)` | `{}` | no |
| <a name="input_temporary_password_validity_days"></a> [temporary\_password\_validity\_days](#input\_temporary\_password\_validity\_days) | (Optional) In the password policy you have set, refers to the number of days a temporary password is valid. If the user does not sign-in during this time, their password will need to be reset by an administrator. | `number` | `1` | no |
| <a name="input_user_device_tracking"></a> [user\_device\_tracking](#input\_user\_device\_tracking) | (Optional) Configure tracking of user devices. Set to 'OFF' to disable tracking, 'ALWAYS' to track all devices or 'USER\_OPT\_IN' to only track when user opts in. | `string` | `"USER_OPT_IN"` | no |
| <a name="input_user_pool_name"></a> [user\_pool\_name](#input\_user\_pool\_name) | (Required) Name of the user pool. | `string` | n/a | yes |
| <a name="input_username_attributes"></a> [username\_attributes](#input\_username\_attributes) | (Optional) Specifies whether email addresses or phone numbers can be specified as usernames when a user signs up. Conflicts with alias\_attributes. | `set(string)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_client_secrets"></a> [client\_secrets](#output\_client\_secrets) | The secrets of all created Cognito User Pool Client resources. |
| <a name="output_clients"></a> [clients](#output\_clients) | All Cognito User Pool Client resources associated with the Cognito User Pool. |
| <a name="output_domain"></a> [domain](#output\_domain) | The full `aws_cognito_user_pool` object. |
| <a name="output_identity_pool_data"></a> [identity\_pool\_data](#output\_identity\_pool\_data) | n/a |
| <a name="output_module_enabled"></a> [module\_enabled](#output\_module\_enabled) | Whether the module is enabled |
| <a name="output_user_pool"></a> [user\_pool](#output\_user\_pool) | The full `aws_cognito_user_pool` object. |
