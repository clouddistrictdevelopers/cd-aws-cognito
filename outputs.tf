# ------------------------------------------------------------------------------
# https://github.com/mineiros-io/terraform-aws-cognito-user-pool
# OUTPUT CALCULATED VARIABLES (prefer full objects)
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# OUTPUT ALL RESOURCES AS FULL OBJECTS
# ------------------------------------------------------------------------------
output "user_pool" {
  description = "The full `aws_cognito_user_pool` object."
  value       = module.aws_cognito.user_pool
}

output "domain" {
  description = "The full `aws_cognito_user_pool` object."
  value       = module.aws_cognito.domain
}

output "clients" {
  description = "All Cognito User Pool Client resources associated with the Cognito User Pool."
  value       = module.aws_cognito.clients
}

output "client_secrets" {
  description = "The secrets of all created Cognito User Pool Client resources."
  value       = module.aws_cognito.clients
  sensitive   = true
}

# ------------------------------------------------------------------------------
# OUTPUT MODULE CONFIGURATION
# ------------------------------------------------------------------------------

output "module_enabled" {
  description = "Whether the module is enabled"
  value       = var.module_enabled
}

 output "identity_pool_data" {
   value = module.cognito_identity_pool.identity_pool
 }
